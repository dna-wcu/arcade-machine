package cs151.arcade;

import cs151.arcade.main.ArcadeMachine;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/5/2014
 */
public class Start {

    public static void main(String[] args) {
        try {
            AppGameContainer container =
                    new AppGameContainer(new ArcadeMachine());
            container.setDisplayMode(ArcadeMachine.GAME_WIDTH,
                                     ArcadeMachine.GAME_HEIGHT,
                                     false);
            container.setTargetFrameRate(60);
            container.setShowFPS(false);
            container.start();
        } catch (SlickException e) {
            System.out.println("**** Something went really wrong");
            System.out.println(e.getMessage());
        }
    }

}
