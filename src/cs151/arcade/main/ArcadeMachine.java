package cs151.arcade.main;

import cs151.arcade.states.MainMenuState;
import cs151.arcade.states.SplashState;
import cs151.arcade.states.States;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/5/2014
 */
public class ArcadeMachine extends StateBasedGame {

    public static final int GAME_WIDTH = 640;
    public static final int GAME_HEIGHT = 480;

    /**
     * Create a new ArcadeMachine
     */
    public ArcadeMachine() {
        super("Arcade Machine");
    }

    /**
     * Initialise the list of states making up this game
     *
     * @param container The container holding the game
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise the state based game resources
     */
    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        addState(new SplashState());
        addState(new MainMenuState());
        enterState(States.SPLASH.getID());
    }
}
