package cs151.arcade.states;

import com.sun.org.apache.bcel.internal.generic.LASTORE;
import cs151.arcade.main.ArcadeMachine;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 * The game state that displays prior information about the authors of this
 * program.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/5/2014
 */
public class SplashState extends BasicGameState {

    /**
     * Amount of time it takes for the first splash screen to end
     */
    private static int FIRST_SPLASH = 1500;

    /**
     * Amount of time it takes for the second splash screen
     */
    private static int SECOND_SPLASH = 1500;

    /**
     * The image that is the title logo of our game
     */
    private Image title;

    /**
     * Daniel and my logo for the game
     */
    private Image logo;

    /**
     * Amount of time elapsed in the game state
     */
    private int elapsed;

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.SPLASH.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        logo = new Image("res/images/arc_logo.png").getScaledCopy(0.5f);
        title = new Image("res/images/splash.png").getScaledCopy(0.5f);
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game, Graphics g) throws SlickException {
        if (elapsed < FIRST_SPLASH) {
            title.draw(ArcadeMachine.GAME_WIDTH / 2 - logo.getWidth() / 2,
                    ArcadeMachine.GAME_HEIGHT / 2 - logo.getHeight());
        } else {
            logo.draw(ArcadeMachine.GAME_WIDTH / 2 - logo.getWidth() / 2,
                    ArcadeMachine.GAME_HEIGHT / 2 - logo.getHeight());
        }
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the  standard framework mechanism
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if ((elapsed += delta) > FIRST_SPLASH + SECOND_SPLASH) {
            game.enterState(States.MAIN_MENU.getID(),
                            new FadeOutTransition(),
                            new FadeInTransition());
        }
    }
}
