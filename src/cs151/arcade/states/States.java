package cs151.arcade.states;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/5/2014
 */
public enum States {

    SPLASH(0),
    MAIN_MENU(1);

    private final int ID;

    private States(int id) {
        ID = id;
    }

    public int getID() {
        return ID;
    }
}
