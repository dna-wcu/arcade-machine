package cs151.arcade.states;

import cs151.arcade.main.ArcadeMachine;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.io.IOException;

/**
 * This is the Class for the main menu state.  It is the interface between
 * the user and the games so that the user can run our games without the
 * use of the command prompt lines to run the jar files.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/5/2014
 */
public class MainMenuState extends BasicGameState{

    enum SubState {
        SET, ANIMATING
    }

    private static Runtime rt = Runtime.getRuntime();

    /** This is the logo for the arcade machine */
    private Image logo;
    /** This is the the slide underneath the title */
    private Rectangle overlayTop;
    /** Slides of the games */
    private Frame[] frames;
    /** states of the slide animation */
    private SubState subState;
    /** This is the frame the user is currently on*/
    private int currentFrame;
    /** The default x position where the frames stop */
    private int defaultX;
    /** The default y position where the frames stop */
    private int defaultY;
    /** Current frame of the game you are on */
    private Frame current;
    /** frame after the current one */
    private Frame next;
    /** frame before the current one */
    private Frame prev;
    /** true when moving to the next frame */
    private boolean changingNext;
    /** true when moving to the previous frame */
    private boolean changingPrev;

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.MAIN_MENU.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container, StateBasedGame game) throws
            SlickException {
        logo = new Image("res/images/title.png").getScaledCopy(512, 100);
        overlayTop = new Rectangle(0, 0, ArcadeMachine.GAME_WIDTH, 100);

        float percent = 0.7f;
        int scaledWidth = (int) (ArcadeMachine.GAME_WIDTH * percent);
        int scaledHeight = (int) (ArcadeMachine.GAME_HEIGHT * percent);
        Image[] frameImgs = new Image[]{
                new Image("res/previews/asteroids.png")
                        .getScaledCopy(scaledWidth, scaledHeight),
                new Image("res/previews/pong.png")
                        .getScaledCopy(scaledWidth, scaledHeight),
                new Image("res/previews/snake.png")
                        .getScaledCopy(scaledWidth, scaledHeight),
                new Image("res/previews/breakout.png")
                        .getScaledCopy(scaledWidth, scaledHeight)
        };

        frames = new Frame[frameImgs.length];
        int index = 0;
        for (Image img : frameImgs) {
            frames[index] = new Frame();
            frames[index++].image = img;
        }
        frames[0].name = "Asteroids";
        frames[1].name = "Pong";
        frames[2].name = "Snake";
        frames[3].name = "Breakout";

        currentFrame = 0;
        subState = SubState.SET;

        defaultX = (ArcadeMachine.GAME_WIDTH
                -  frames[currentFrame].getWidth()) / 2;
        defaultY = 110;


        prev = frames[(currentFrame - 1) == -1 ? frames.length - 1 :
                currentFrame - 1];
        current = frames[currentFrame];
        next = frames[(currentFrame + 1) % frames.length];
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render
     * an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        // Draw the frame that is selected
        if (subState == SubState.SET) {
            current.draw(g, defaultX, defaultY);
        } else {
            final int scrollSpeed = 20;

            if (changingNext) {
                current.y -= scrollSpeed;
                next.y -= scrollSpeed;

                if (next.y <= defaultY) {
                    next.y = defaultY;
                    currentFrame++;
                    current = next;
                    next = frames[(currentFrame + 1) % frames.length];
                    subState = SubState.SET;
                    changingNext = changingPrev = false;
                    current.draw(g, defaultX, defaultY);
                } else {
                    current.draw(g, defaultX, current.y);
                    next.draw(g, defaultX, next.y);
                }
            } else if (changingPrev) {
                current.y += scrollSpeed;
                prev.y += scrollSpeed;

                if (prev.y >= defaultY) {
                    prev.y = defaultY;
                    currentFrame--;
                    current = prev;

                    int prevFrame
                            = currentFrame - 1 < 0 ? frames.length - 1
                                                   : currentFrame - 1;
                    prev = frames[prevFrame];
                    subState = SubState.SET;
                    changingNext = changingPrev = false;
                    current.draw(g, defaultX, defaultY);
                } else {
                    current.draw(g, defaultX, current.y);
                    prev.draw(g, defaultX, prev.y);
                }
            }
        }
        // Draw the logo in the top left corner
        g.setColor(Color.black);
        g.fill(overlayTop);
        g.setColor(Color.white);
        logo.draw(0, 0);
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        Input input = container.getInput();

        if (currentFrame < 0) {
            currentFrame = frames.length - 1;
        }
        currentFrame %= frames.length;

        prev = frames[(currentFrame - 1) == -1 ? frames.length - 1 :
                currentFrame - 1];
        current = frames[currentFrame];
        next = frames[(currentFrame + 1) % frames.length];

        if (subState != SubState.ANIMATING) {
            if (input.isKeyPressed(Input.KEY_DOWN)) {
                nextFrame();
                subState = SubState.ANIMATING;
            } else if (input.isKeyPressed(Input.KEY_UP)) {
                prevFrame();
                subState = SubState.ANIMATING;
            } else if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
            } else if (input.isKeyPressed(Input.KEY_ENTER)) {
                launchGame(currentFrame);
            }
        }
    }

    /**
     * moves to the next frame
     */
    private void nextFrame() {
        next.y = 480;
        changingNext = true;
    }

    /**
     * moves to prev frame
     */
    private void prevFrame() {
        prev.y = -prev.getHeight();
        changingPrev = true;
    }

    /** Launches the game associated with the current frame
     *
     * @param currentFrame
     */
    private void launchGame(int currentFrame) {
        Process process = null;
        String javaCmd = "java -jar ";
        try {
            switch (currentFrame) {
                case 0:
                    process = rt.exec(javaCmd + "Asteroids.jar");
                    break;
                case 1:
                    process = rt.exec(javaCmd + "Pong.jar");
                    break;
                case 2:
                    process = rt.exec(javaCmd + "Snake.jar");
                    break;
                case 3:
                    process = rt.exec(javaCmd + "Breakout.jar");
                    break;
            }
        } catch (IOException e) {
            System.out.println("**** Failed to execute command");
            System.out.println(e.getMessage());
        } finally {
            if (process != null) {
                try {
                    process.waitFor();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                process.destroy();
            }
        }
    }

    /** This displays the picture of the games */
    private class Frame {
        private int y;
        private Image image;
        private String name;

        /** renders the image */
        private void draw(Graphics g, int x, int y) {
            g.drawString(name, x, y);
            image.draw(x, y + g.getFont().getLineHeight());
        }

        /** gets the width of the image
         *
         * @return int image width
         */
        private int getWidth() {
            return image.getWidth();
        }

        /** gets the height of the image
         *
         * @return int image height
         */
        private int getHeight() {
            return image.getHeight();
        }
    }
}
